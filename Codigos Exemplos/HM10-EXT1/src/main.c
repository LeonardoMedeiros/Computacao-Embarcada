#include "asf.h"

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/


/**
 * \brief Configure UART console.
 * BaudRate : 115200
 * 8 bits
 * 1 stop bit
 * sem paridade
 */

static void configure_console(void)
{

  /* Configura USART1 Pinos */
  sysclk_enable_peripheral_clock(ID_PIOB);
  sysclk_enable_peripheral_clock(ID_PIOA);
  pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4);  // RX
  pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
 	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;
 
	const usart_serial_options_t uart_serial_options = {
		.baudrate   = CONF_UART_BAUDRATE,
		.charlength = CONF_UART_CHAR_LENGTH,
		.paritytype = CONF_UART_PARITY,
		.stopbits   = CONF_UART_STOP_BITS,
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

/**
 * \brief Configure UART console.
 */
static void USART0_init(void){
  
  /* Configura USART0 Pinos */
  sysclk_enable_peripheral_clock(ID_PIOB);
  pio_set_peripheral(PIOB, PIO_PERIPH_C, PIO_PB0); //Tx	Modulo - Rx Atmel
  pio_set_peripheral(PIOB, PIO_PERIPH_C, PIO_PB1); //Rx Modulo - Tx Atmel
  
  /* Configura opcoes USART */
  const sam_usart_opt_t usart_settings = {
    .baudrate     = 9600,
    .char_length  = US_MR_CHRL_8_BIT,
    .parity_type  = US_MR_PAR_NO,
    .stop_bits    = US_MR_NBSTOP_1_BIT,
    .channel_mode = US_MR_CHMODE_NORMAL
  };

  /* Ativa Clock periferico USART0 */
  sysclk_enable_peripheral_clock(ID_USART0);
  
  /* Configura USART para operar em modo RS232 */
  usart_init_rs232(USART0, &usart_settings, sysclk_get_peripheral_hz());
  
  /* Enable the receiver and transmitter. */
	usart_enable_tx(USART0);
	usart_enable_rx(USART0);
 }



static void USART2_init(void){
	
	/* Configura USART0 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOD);
	pio_set_peripheral(PIOD, PIO_PERIPH_B, PIO_PD15); //RX - TXModulo
	pio_set_peripheral(PIOD, PIO_PERIPH_B, PIO_PD16); //TX - RXModulo	
	
	/* Configura opcoes USART */
	const sam_usart_opt_t usart_settings = {
		.baudrate     = 9600,
		.char_length  = US_MR_CHRL_8_BIT,
		.parity_type  = US_MR_PAR_NO,
		.stop_bits    = US_MR_NBSTOP_1_BIT,
		.channel_mode = US_MR_CHMODE_NORMAL
	};

	/* Ativa Clock periferico USART2 */
	sysclk_enable_peripheral_clock(ID_USART2);
	
	/* Configura USART para operar em modo RS232 */
	usart_init_rs232(USART2, &usart_settings, sysclk_get_peripheral_hz());
	
	/* Enable the receiver and transmitter. */
	usart_enable_tx(USART2);
	usart_enable_rx(USART2);
}

/**
 *  Envia para o UART uma string
 */
uint32_t usart_putString(Usart* usart, uint8_t *pstring){
  uint32_t i = 0 ;

  while(*(pstring + i)){
    usart_serial_putchar(usart, *(pstring+i++));
    while(!uart_is_tx_empty(usart)){};
  }    
     
  return(i);
}

/*
 * Busca no UART uma string
 */
uint32_t usart_getString(Usart*  usart, uint8_t *pstring){
  uint32_t i = 0 ;
  
  usart_serial_getchar(usart, (pstring+i));
  while(*(pstring+i) != '\n'){
	printf("%c",*(pstring+i));
	usart_serial_getchar(usart, (pstring+(++i)));
  }
  *(pstring+i)= 0x00;
  return(i);
}

void confg_Blt1()
{
	//Configure Uart
	usart_putString(USART0, "AT");
	usart_putString(USART0, "AT");
	usart_putString(USART0, "AT");
	delay_ms(800);
  
	usart_putString(USART0, "AT+RESET");
	delay_ms(800);
	
	usart_putString(USART0, "AT+NAMEMesaTest1");
	delay_ms(800);
  
	/* Bluetooth 4.0
	usart_putString(USART0, "AT+TYPE1");
	delay_ms(800);
  
    usart_putString(USART0, "AT+ROLEM");
	delay_ms(800);
	*/
	
	usart_putString(USART0, "AT+PIN101010");
	delay_ms(800);
  
	};


void confg_Blt2()
{
	//Configure Uart
	usart_putString(USART0, "AT");
	usart_putString(USART0, "AT");
	usart_putString(USART2, "AT");
	delay_ms(800);
  
	usart_putString(USART2, "AT+RESET");
	delay_ms(800);
	
	usart_putString(USART2, "AT+NAMEMesaTest2");
	delay_ms(800);
  
	/* Bluetooth 4.0
	usart_putString(USART2, "AT+TYPE1");
	delay_ms(800);
  
    usart_putString(USART2, "AT+ROLEM");
	delay_ms(800);
	*/
	
	usart_putString(USART2, "AT+PIN202020");
	delay_ms(800);
  
	};


/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main(void){
  
	/* buffer para recebimento de dados */
	uint8_t bufferRX[100];
	uint8_t bufferTX[100];

	/* Initialize the SAM system */
	sysclk_init();
   
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
  
	/* Inicializa com serial com PC*/
	configure_console();
  
	/* Configura USART0 para comunicacao com o HM-10 */
	USART0_init();
	USART2_init();
 
	/* Inicializa funcao de delay */
	delay_init( sysclk_get_cpu_hz());
  
	confg_Blt1();
	confg_Blt2();
    
    
	while (1) {
		usart_getString(USART2, bufferRX);
		
		if(bufferRX[0] == '1'){
			sprintf(bufferTX, "%s \n", "Player 1");
			usart_putString(USART2, bufferTX);
		}
		else if(bufferRX[0] == '2'){
			sprintf(bufferTX, "%s \n", "Player 2");
			usart_putString(USART2, bufferTX);
		}
		else{
			sprintf(bufferTX, "%s \n", "Invalido");
			usart_putString(USART2, bufferTX);
			}
	}
}