/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>
#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

#define YEAR        2018
#define MONTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      45
#define SECOND      0


#define LED1_PIO_ID ID_PIOA
#define LED1_PIO PIOA
#define LED1_PIN 0
#define LED1_PIN_MASK (1 << LED1_PIN)

#define BUT_PIO PIOD
#define BUT_PIO_ID ID_PIOD
#define BUT_PIN 28
#define BUT_PIN_MASK (1 << BUT_PIN)
#define BUT_DEBOUNCING_VALUE 79

char s[8];
uint32_t hour, minute, second;
uint32_t second_Alarm;

volatile uint8_t flag_led0 = 0;
volatile uint8_t flag_but1 = 1;
volatile uint8_t flag_alarme = 0;



void led_toggle(Pio *pio, uint32_t mask){
	for(int i = 0; i < 5; i++){
		pio_set(pio,mask);
		delay_ms(40);
		pio_clear(pio, mask);
		delay_ms(40);
		pio_set(pio,mask);
	}
}

void RTC_init(void);
void BUT_init(void);
void LED_init(int estado);
void pin_toggle(Pio *pio, uint32_t mask);

void RTC_init(){
	/* Configure o PMC */
	pmc_enable_periph_clk(ID_RTC);
	
	/* Default RTC configuration, 24-hour mode */
	// rtc_set_hour_mode(RTC, 0);
	
	// Configure date and time
	rtc_set_date(RTC, YEAR, MONTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);
	
	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);
	
	// Activate interruption by alarm and time (every sec)
	rtc_enable_interrupt(RTC, RTC_IER_ALREN | RTC_IER_SECEN);
}

void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);
	
	//Verifica tipo de interrupção
	if((ul_status & RTC_SR_SEC) == RTC_SR_SEC){
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
		uint32_t minuteAt = minute;	
		
		//GET Time
		rtc_get_time(RTC, &hour, &minute, &second);
	
		//Atualiza LCD
		if(minute != minuteAt){
			minuteAt = minute;
			
			//LCD time
			
			sprintf(s,"%d:%d", hour, minute);
			gfx_mono_draw_string(s, 0, 0, &sysfont);
		}			
	}
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		if(second_Alarm == second){
			led_toggle(LED1_PIO, LED1_PIN_MASK);
			flag_alarme = 0;
			second_Alarm = 0;
			
		}
	}
			
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
}

void LED_init(int state){
	
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, state, 0, 0 );
};

static void BUT_Handler(uint32_t id, uint32_t mask)
{
	pio_clear(LED1_PIO, LED1_PIN_MASK);
	if(flag_alarme){
		second_Alarm += 2;
	}
	
	else{
		second_Alarm = second + 2;
		flag_alarme = 1;
	}
	rtc_set_time_alarm(RTC, 1, hour, 1, minute, 1, second_Alarm);
}


void BUT_init(void){
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_set_input(BUT_PIO, BUT_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	
	pio_enable_interrupt(BUT_PIO, BUT_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIN_MASK, PIO_IT_FALL_EDGE, BUT_Handler);
	
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
}

int main (void)
{
	board_init();
	sysclk_init();
	delay_init();

	gfx_mono_ssd1306_init();
	gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);	

	BUT_init();
	LED_init(1);
	RTC_init();

	while(1) {

	}
}
=======
/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>

#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

int main (void)
{
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();
	
	gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
    gfx_mono_draw_string("14:03PM", 0, 0, &sysfont);
	
	while(1) {
	
	}
}
