#include "asf.h"

/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/

/**
 *  Informacoes para o RTC
 *  poderia ser extraida do __DATE__ e __TIME__
 *  ou ser atualizado pelo PC.
 */
#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      45
#define SECOND      0

/**
* LEDs
*/

#define LED1_PIO_ID	   ID_PIOA
#define LED1_PIO        PIOA
#define LED1_PIN		   0
#define LED1_PIN_MASK   (1<<LED1_PIN)

#define LED2_PIO_ID	   ID_PIOC
#define LED2_PIO        PIOC
#define LED2_PIN		   30
#define LED2_PIN_MASK   (1<<LED2_PIN)

#define LED3_PIO_ID	   ID_PIOB
#define LED3_PIO        PIOB
#define LED3_PIN		   2
#define LED3_PIN_MASK   (1<<LED3_PIN)

/**
* Botao
*/
#define BUT1_PIO_ID			  ID_PIOD
#define BUT1_PIO				  PIOD
#define BUT1_PIN				  28
#define BUT1_PIN_MASK			  (1 << BUT1_PIN)
#define BUT1_DEBOUNCING_VALUE  79

#define BUT2_PIO_ID			  ID_PIOC
#define BUT2_PIO				  PIOC
#define BUT2_PIN				  31
#define BUT2_PIN_MASK			  (1 << BUT2_PIN)
#define BUT2_DEBOUNCING_VALUE  79

#define BUT3_PIO_ID			  ID_PIOA
#define BUT3_PIO				  PIOA
#define BUT3_PIN				  19
#define BUT3_PIN_MASK			  (1 << BUT3_PIN)
#define BUT3_DEBOUNCING_VALUE  79


/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint8_t flag_but1 = 0;
volatile uint8_t flag_but2 = 0;
volatile uint8_t flag_but3 = 0;

//int Minute = MINUTE;

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void BUT_init(void);
void LED_init(int estado);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void RTC_init(void);
void pin_toggle(Pio *pio, uint32_t mask);

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

/**
*  Handle Interrupcao botao 1
*/
static void Button1_Handler(uint32_t id, uint32_t mask)
{	
	flag_but1 = ! flag_but1;

	if(flag_but1){
		tc_start(TC0,0);
	}
	else{
		tc_stop(TC0,0);
	}
		
}

static void Button2_Handler(uint32_t id, uint32_t mask)
{
	flag_but2 = ! flag_but2;

	if(flag_but2){
		tc_start(TC0,1);
	}
	else{
		tc_stop(TC0,1);
	}
}

static void Button3_Handler(uint32_t id, uint32_t mask)
{
	flag_but3 = ! flag_but3;
	
	if(flag_but3){
		tc_start(TC0,2);
	}
	else{
		tc_stop(TC0,2);
	}	
}

/**
*  Interrupt handler for TC1 interrupt.
*/
void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrupo foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_but1){
		pin_toggle(LED1_PIO, LED1_PIN_MASK);
	}
}

void TC1_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrupo foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_but2){
		pin_toggle(LED2_PIO, LED2_PIN_MASK);
	}
}

void TC2_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrupo foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 2);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_but3){
		pin_toggle(LED3_PIO, LED3_PIN_MASK);
	}
}

/**
* \brief Interrupt handler for the RTC. Refresh the display.
*/


/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/**
*  Toggle pin controlado pelo PIO (out)
*/
void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
		pio_clear(pio, mask);
	else
		pio_set(pio,mask);
}


void but1_init(){
	pmc_enable_periph_clk(BUT1_PIO_ID);
	pio_set_input(BUT1_PIO, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);// INTERRUPCAO
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);
	
	pio_get_interrupt_status(BUT1_PIO);
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 1);
		
}

void but2_init(){
	pmc_enable_periph_clk(BUT2_PIO_ID);
	pio_set_input(BUT2_PIO, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);// INTERRUPCAO
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE, Button2_Handler);

	pio_get_interrupt_status(BUT2_PIO);
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 1);
}

void but3_init(){
	pmc_enable_periph_clk(BUT3_PIO_ID);
	pio_set_input(BUT3_PIO, BUT3_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	pio_enable_interrupt(BUT3_PIO, BUT3_PIN_MASK);// INTERRUPCAO
	pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN_MASK, PIO_IT_FALL_EDGE, Button3_Handler);

	pio_get_interrupt_status(BUT3_PIO);
	NVIC_EnableIRQ(BUT3_PIO_ID);
	NVIC_SetPriority(BUT3_PIO_ID, 1);
	
}

void BUT_init(void){
	but1_init();
	but2_init();
	but3_init();
}


/**
* @Brief Inicializa o pino do LED
*/
void LED_init(int estado){
	
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, estado, 0, 0 );
	
	pmc_enable_periph_clk(LED2_PIO_ID);
	pio_set_output(LED2_PIO, LED2_PIN_MASK, estado, 0, 0 );
	
	pmc_enable_periph_clk(LED3_PIO_ID);
	pio_set_output(LED3_PIO, LED3_PIN_MASK, estado, 0, 0 );
};

/**
* Configura TimerCounter (TC) para gerar uma interrupcao no canal (ID_TC e TC_CHANNEL)
* na taxa de especificada em freq.
*/
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrupco no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrupco no TC canal 0 */
	/* Interrupo no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}


/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main(void){
	/* Initialize the SAM system */
	sysclk_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;

	/* Configura Leds */
	LED_init(0);

	/* Configura os botes */
	BUT_init();

	/** Configura timer TC0, canal 1 */
	TC_init(TC0, ID_TC0, 0, 8);
	TC_init(TC0, ID_TC1, 1, 11);
	TC_init(TC0, ID_TC2, 2, 17);


	while (1) {
		/* Entrar em modo sleep */
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
	}
}
