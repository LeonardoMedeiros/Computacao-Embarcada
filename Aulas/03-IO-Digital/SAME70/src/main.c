/**
 * 5 semestre - Eng. da Computao - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"

/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO
#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)
#define BUT_PIO PIOA

/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/
void pisca_Led(Pio *pio, uint32_t mask  ){
	for(int counter = 0; counter < 5; counter++){
		pio_set(pio, mask);
		delay_ms(200);
		pio_clear(pio, mask);
		delay_ms(200);
	}
}

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	sysclk_init(Pio *p, );
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	//Enable
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	//Configure Pio
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);

	
	// super loop
	while (1) {
		
		int signal_button = pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK);
			if(signal_button == 0){
					pisca_Led();	
				}
			
			else {
				pio_set(LED_PIO, LED_PIO_PIN_MASK);
				}
			}
			
	return 0;
}
