#include <asf.h>
#include "conf_board.h"

#define configASSERT_DEFINED 1

#define TASK_MONITOR_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_MONITOR_STACK_PRIORITY        (tskIDLE_PRIORITY)
#define TASK_LED_STACK_SIZE                (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY            (tskIDLE_PRIORITY)

/* LED */

#define LED1_PIO_ID	   ID_PIOA
#define LED1_PIO        PIOA
#define LED1_PIN		   0
#define LED1_PIN_MASK   (1<<LED1_PIN)

#define LED2_PIO_ID	   ID_PIOC
#define LED2_PIO        PIOC
#define LED2_PIN		   30
#define LED2_PIN_MASK   (1<<LED2_PIN)

#define LED3_PIO_ID	   ID_PIOB
#define LED3_PIO        PIOB
#define LED3_PIN		   2
#define LED3_PIN_MASK   (1<<LED3_PIN)


/* Botao */
#define BUT1_PIO_ID			  ID_PIOD
#define BUT1_PIO				  PIOD
#define BUT1_PIN				  28
#define BUT1_PIN_MASK			  (1 << BUT1_PIN)
#define BUT1_DEBOUNCING_VALUE  79

#define BUT2_PIO_ID			  ID_PIOC
#define BUT2_PIO				  PIOC
#define BUT2_PIN				  31
#define BUT2_PIN_MASK			  (1 << BUT2_PIN)
#define BUT2_DEBOUNCING_VALUE  79

#define BUT3_PIO_ID			  ID_PIOA
#define BUT3_PIO				  PIOA
#define BUT3_PIN				  19
#define BUT3_PIN_MASK			  (1 << BUT3_PIN)
#define BUT3_DEBOUNCING_VALUE  79


extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);


void but_callback(void);
void init_but_board(void);

/** Semaforo a ser usado pela task led */
SemaphoreHandle_t xSemaphore1;
SemaphoreHandle_t xSemaphore2;
SemaphoreHandle_t xSemaphore3;


volatile uint8_t flag_LED1 = 0;
volatile uint8_t flag_LED2 = 0;
volatile uint8_t flag_LED3 = 0;

/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}

/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void)
{
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}


void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}


void LED_init(int estado){
	
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, estado, 0, 0 );
	
	pmc_enable_periph_clk(LED2_PIO_ID);
	pio_set_output(LED2_PIO, LED2_PIN_MASK, estado, 0, 0 );
	
	pmc_enable_periph_clk(LED3_PIO_ID);
	pio_set_output(LED3_PIO, LED3_PIN_MASK, estado, 0, 0 );
};


/**
 * \brief This task, when activated, send every ten seconds on debug UART
 * the whole report of free heap and total tasks status
 */
static void task_monitor(void *pvParameters)
{
	static portCHAR szList[256];
	UNUSED(pvParameters);

	for (;;) {
		printf("--- task ## %u", (unsigned int)uxTaskGetNumberOfTasks());
		vTaskList((signed portCHAR *)szList);
		printf(szList);
		vTaskDelay(1000);
	}
}

/**
 * \brief This task, when activated, make LED blink at a fixed rate
 */
static void task_led1(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	xSemaphore1 = xSemaphoreCreateBinary();

	if (xSemaphore1 == NULL)
		printf("falha em criar o semaforo \n");
	
	const TickType_t xDelay = 250 / portTICK_PERIOD_MS;
	for (;;) {
	
		if (flag_LED1){
			pin_toggle(LED1_PIO, LED1_PIN_MASK);
		}
	
		if( xSemaphoreTake(xSemaphore1, ( TickType_t ) 0) == pdTRUE ){
			flag_LED1 = !flag_LED1; 
			
			if(!flag_LED1){
				pio_set(LED1_PIO, LED1_PIN_MASK);
			}
			
		}
		vTaskDelay(xDelay);
	
	}
}

static void task_led2(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	xSemaphore2 = xSemaphoreCreateBinary();

	if (xSemaphore2 == NULL)
	printf("falha em criar o semaforo \n");
	
	const TickType_t xDelay = 150 / portTICK_PERIOD_MS;
	for (;;) {
		
		if (flag_LED2){
			pin_toggle(LED2_PIO, LED2_PIN_MASK);
			
		}
		
		if( xSemaphoreTake(xSemaphore2, ( TickType_t ) 0) == pdTRUE ){
			flag_LED2 = !flag_LED2;
			
			if(!flag_LED2){
				pio_set(LED2_PIO, LED2_PIN_MASK);
			}
		}
		vTaskDelay(xDelay);
	}
}

static void task_led3(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	xSemaphore3 = xSemaphoreCreateBinary();

	if (xSemaphore3 == NULL)
	printf("falha em criar o semaforo \n");
	

	const TickType_t xDelay = 50 / portTICK_PERIOD_MS;
	for (;;) {
		
		if (flag_LED3){
			pin_toggle(LED3_PIO, LED3_PIN_MASK);
			
		}
		
		if( xSemaphoreTake(xSemaphore3, ( TickType_t ) 0) == pdTRUE ){
			flag_LED3 = !flag_LED3;
			
			if(!flag_LED3){
				pio_set(LED3_PIO, LED3_PIN_MASK);
			}
			
		}
		vTaskDelay(xDelay);
	}
}



void but_callback1(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback 1 \n");
	xSemaphoreGiveFromISR(xSemaphore1, &xHigherPriorityTaskWoken);
	printf("semafaro tx \n");
}

void but_callback2(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback 2 \n");
	xSemaphoreGiveFromISR(xSemaphore2, &xHigherPriorityTaskWoken);
	printf("semafaro tx \n");
}

void but_callback3(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback 3 \n");
	xSemaphoreGiveFromISR(xSemaphore3, &xHigherPriorityTaskWoken);
	printf("semafaro tx \n");
}



void init_but_1(void){
	/* conf bot�o como entrada */
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 8);
	pio_configure(BUT1_PIO, PIO_INPUT, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT1_PIO, BUT1_PIN_MASK, 60);
	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE , but_callback1);
}

void init_but_2(void){
	/* conf bot�o como entrada */
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 8);
	pio_configure(BUT2_PIO, PIO_INPUT, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT2_PIO, BUT2_PIN_MASK, 60);
	pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE , but_callback2);
}

void init_but_3(void){
	/* conf bot�o como entrada */
	NVIC_EnableIRQ(BUT3_PIO_ID);
	NVIC_SetPriority(BUT3_PIO_ID, 8);
	pio_configure(BUT3_PIO, PIO_INPUT, BUT3_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT3_PIO, BUT3_PIN_MASK, 60);
	pio_enable_interrupt(BUT3_PIO, BUT3_PIN_MASK);
	pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN_MASK, PIO_IT_FALL_EDGE , but_callback3);
}

void init_but_board(){
	init_but_1();
	init_but_2();
	init_but_3();
}

/**
 * \brief Configure the console UART.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Specify that stdout should not be buffered. */
#if defined(__GNUC__)
	setbuf(stdout, NULL);
#else
	/* Already the case in IAR's Normal DLIB default configuration: printf()
	 * emits one character at a time.
	 */
#endif
}

/**
 *  \brief FreeRTOS Real Time Kernel example entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
int main(void)
{
	/* Initialize the SAM system */
	sysclk_init();
	board_init();

	/* Initialize the console uart */
	configure_console();

	/* Output demo information. */
	printf("-- Freertos Example --\n\r");
	printf("-- %s\n\r", BOARD_NAME);
	printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);

	/* iniciliza botao */
	init_but_board();
	
	/* iniciliza LED */
	LED_init(1);

	/* Create task to monitor processor activity */
	if (xTaskCreate(task_monitor, "Monitor", TASK_MONITOR_STACK_SIZE, NULL,
			TASK_MONITOR_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Monitor task\r\n");
	}

	/* Create task to make led blink */
	if (xTaskCreate(task_led1, "Led1", TASK_LED_STACK_SIZE, NULL,
			TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_led2, "Led2", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_led3, "Led3", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}


	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;
}
